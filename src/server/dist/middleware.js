var server = require('server');
var modern = server.utils.modern;
var _a = require('server/reply'), render = _a.render, redirect = _a.redirect, file = _a.file, header = _a.header;
module.exports = [
    function (ctx) { return header('Access-Control-Allow-Origin', '*'); },
    //ctx => header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Content-Type'),
    function (ctx) { return header('Access-Control-Allow-Methods', 'GET, HEAD, PUT, PATCH, POST, DELETE'); }
];
/*
modern(require('cors')({
        origin: '*',
        optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
      }))
*/ 
