import { User } from "../entity/User";

const {get,post,put,del} = require('server').router;
module.exports=[
    get('/', ctx => 'Hello world'),
    post('/', ctx => {
      console.log(ctx.data);
      return 'ok';
    }),
    get('/api', ctx => {
      return 'api';
    }),
    get('/abc',ctx=>{
      console.log(ctx);
    }),
    post('/login',ctx=>{
      ctx.username;

    }),
    get('/requestVacation',ctx=>{
      console.log(ctx);
      // employee
      // von wann bis wann
      //const vacation = new Vacation();
      //vacation.firstName = "Timber";
      //vacation.lastName = "Saw";
      //vacation.age = 25;
      let obj = {user:'Artur', id: 5};
      return JSON.stringify(obj);
      return 'requestVacation'
    }),
    post('/requestVacation',ctx=>{
      console.log(ctx);
      // employee
      // von wann bis wann
      //const vacation = new Vacation();
      //vacation.firstName = "Timber";
      //vacation.lastName = "Saw";
      //vacation.age = 25;
      return 'requestVacation'
    }),
    
     ///////////////////////////////////////////////
     post('/createUser', ctx =>{
      // id: Number
      // email: String
      // username: String
      // password: String
      // employee: Boolean
      // supervisor: Boolean
      // HR: Boolean
      // supervisedByID: Number
      let newUser = new User();
      
    }),
    post('/changeUser', ctx =>{
      // id: Number
      // email: String
      // username: String
      // password: String
      // employee: Boolean
      // supervisor: Boolean
      // HR: Boolean
    }),
    post('/login', ctx =>{
      // username: String
      // password: String
    }),
    get('/loginAuthentication', ctx =>{
      // loginValid: Boolean
    }),

    //Employee:
    post('/enterWorktime', ctx =>{
      // id: Number
      // startTime: Date
      // endTime: Date
      // pauseTime: Number
    }),
    post('/submitTimesheet', ctx =>{
      // id: Number
      // month: Number
      // year: Number
    }),
    post('/submitVacation', ctx =>{
      // id: Number
      // startDate: Date
      // endDate: Date
    }),
    post('/cancelVacation', ctx =>{
      // id: Number
      // startDate: Date
      // endDate: Date
    }),
    get('/viewTimesheet', ctx =>{
      // workdays: workday[]
    }),

    //Supervisor:
    post('/reviewTimesheet', ctx =>{
      // id: Number
      // month: Number
      // year: Number
      // approve: Boolean
      // reject: Boolean
      // reason: String
    }),
    post('/reviewVacation', ctx =>{
      // id: Number
      // startDate: Date
      // endDate: Date
      // approve: Boolean
      // reject: Boolean
      // reason: String
    }),
    get('/viewEmployees', ctx =>{
      // ids: Number[]
      // email: String[]
      // username: String[]
    }),
    get('/viewTimesheet', ctx =>{
      // id: Number[]
      // month: Number[]
      // approve: Boolean[]
      // reject: Boolean[]
      // reason: String[]
    }),
    //HR:
    post('/enterSickness', ctx =>{
      // id: Number
      // startDate: Date
      // endDate: Date
      // reason: String
    }),
    get('/viewEmployees', ctx =>{
      // id: Number[]
      // username: String[]
      // flexTime: number[]
    })
  ]