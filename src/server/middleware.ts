const server = require('server');
const { modern } = server.utils;
const { render, redirect, file, header } = require('server/reply');
module.exports=[
    ctx => header('Access-Control-Allow-Origin', '*'),
    //ctx => header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Content-Type'),
    ctx => header('Access-Control-Allow-Methods', 'GET, HEAD, PUT, PATCH, POST, DELETE')
]
/*
modern(require('cors')({
        origin: '*',
        optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
      }))
*/ 