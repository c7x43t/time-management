import "reflect-metadata";
import {createConnection} from "typeorm";
import {User} from "./entity/User";
import {Hr} from "./entity/Hr";
const server = require('server');
const middleware = require('./server/middleware');
const routes = require('./server/routes');
const PORT = 8082;

createConnection().then(async connection => {

    console.log("Inserting a new user into the database...");
    const user = new User();
    user.firstName = "Timber";
    user.lastName = "Saw";
    user.age = 25;
    await connection.manager.save(user);
    console.log("Saved a new user with id: " + user.id);

    console.log("Loading users from the database...");
    const users = await connection.manager.find(User);
    console.log("Loaded users: ", users);

    const hr = new Hr();
    hr.firstName = "Timber";
    hr.lastName = "Saw";
    hr.age = 25;
    // await connection.manager.save(hr);
    const hrs = await connection.manager.find(Hr);
    console.log('Hrs:', hrs);
    console.log("Here you can setup and run express/koa/any other framework.");

// comment
console.log(middleware,routes)
    server({ port: PORT },middleware,routes);
}).catch(error => console.log(error));

