import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity()
export class Vacation {

    @PrimaryGeneratedColumn()
    id: number

    @Column()
    startDate: Date;

    @Column()
    endDate: Date;

    @Column()
    approved: boolean;

    @Column()
    rejected: boolean;

    @Column()
    comment: string;

}
