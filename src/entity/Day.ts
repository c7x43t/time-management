import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity()
export class Day {

    @PrimaryGeneratedColumn()
    id: number

    @Column()
    date: Date;

    @Column()
    weekday: string;

    @Column()
    begin: number;

    @Column()
    end: number;

    @Column()
    break: number;

    @Column()
    target: number;

    @Column()
    absence: number;

    @Column()
    comment: string;

}
