"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.Day = void 0;
var typeorm_1 = require("typeorm");
var Day = /** @class */ (function () {
    function Day() {
    }
    __decorate([
        typeorm_1.PrimaryGeneratedColumn()
    ], Day.prototype, "id");
    __decorate([
        typeorm_1.Column()
    ], Day.prototype, "date");
    __decorate([
        typeorm_1.Column()
    ], Day.prototype, "weekday");
    __decorate([
        typeorm_1.Column()
    ], Day.prototype, "begin");
    __decorate([
        typeorm_1.Column()
    ], Day.prototype, "end");
    __decorate([
        typeorm_1.Column()
    ], Day.prototype, "break");
    __decorate([
        typeorm_1.Column()
    ], Day.prototype, "target");
    __decorate([
        typeorm_1.Column()
    ], Day.prototype, "absence");
    __decorate([
        typeorm_1.Column()
    ], Day.prototype, "comment");
    Day = __decorate([
        typeorm_1.Entity()
    ], Day);
    return Day;
}());
exports.Day = Day;
