"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.Vacation = void 0;
var typeorm_1 = require("typeorm");
var Vacation = /** @class */ (function () {
    function Vacation() {
    }
    __decorate([
        typeorm_1.PrimaryGeneratedColumn()
    ], Vacation.prototype, "id");
    __decorate([
        typeorm_1.Column()
    ], Vacation.prototype, "startDate");
    __decorate([
        typeorm_1.Column()
    ], Vacation.prototype, "endDate");
    __decorate([
        typeorm_1.Column()
    ], Vacation.prototype, "approved");
    __decorate([
        typeorm_1.Column()
    ], Vacation.prototype, "rejected");
    __decorate([
        typeorm_1.Column()
    ], Vacation.prototype, "comment");
    Vacation = __decorate([
        typeorm_1.Entity()
    ], Vacation);
    return Vacation;
}());
exports.Vacation = Vacation;
