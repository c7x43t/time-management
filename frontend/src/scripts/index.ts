import 'regenerator-runtime/runtime'
import ky from 'ky';

(async () => {
	const parsed = await ky.post('http://localhost:8082/requestVacation', {json: {foo: true}}).json();

	console.log(parsed);
	//=> `{data: '🦄'}`
})();