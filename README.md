# Awesome Project Build with TypeORM

Steps to run this project:

1. Run `npm i` command
2. Setup database settings inside `ormconfig.json` file
3. Run `npm start` command

Install instructions:
1.
(windows) https://github.com/coreybutler/nvm/releases
nvm install node
nvm use latest
2.
npm install -g yarn
3.
yarn global add parcel
setx path "%path%;c:\users\YOURUSERNAME\appdata\local\yarn\bin"

Projekt einrichten:
- 1 git clone https://gitlab.com/c7x43t/time-management.git
- 2 cd projektordner
- yarn install

Programmieren:
- 1 Push/Pull über Git Kraken (stage,commit,push)
- 2 Datenbank starten: cd .../db
- docker-compose up
- 3 Projekt starten: Terminal -> New Terminal
- npm start
- 4 http://localhost:8082/

Dokumentation:
- https://serverjs.io/documentation/
- https://typeorm.io/#/

-Test Push
-Test Push2


Frontend (visuell)
    JS: requests -> REST API
    REST API analog zu Funktionsaufrufen
    Daten werden gerendert (?wie)
Backend
    (server.js) Hört und antwortet auf REST API Anfragen (HTTP requets)
    Rest calls interagieren mit der Datenbank (get/set/modify)
    Tabellen sind in entity definiert und nutzen tyeorm (library)
